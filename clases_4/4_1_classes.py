# Класс  -- это элемент, который описывает тип данных.
# Класс можно воспринимать как шаблон для создания объекта.


# Объект -- это экземпляр класса.


# Как обьявить класс.

# сlass Название_класса:
class Fruit:
    # атрибуты класса
    name = None
    color = None
    value = 1.0

    # Метод
    def __init__(self, name, color):
        self.name = name
        self.color = color

    # Метод
    def bite(self):
        if self.value > 0.0:
            self.value -= 0.3
            print('Укус на {}.'.format(0.3))
            return True

        return False


apple = Fruit(name='apple', color='red')
# print(type(apple))

# print(apple.name)
# print(apple.color)
# print(apple.value)


# apple.bite()
# print(apple.value)




# Наследование.
# При наследовании дочерний класс принимает все атрибуты и методы родительского класса.

class Peel:
    has_peel = True

    def clean_out(self):
        self.has_peel = False

# В скобках указываем классы, от которых хотим наследоваться.
# Порядок наследования важен!!!

class Citrus(Peel, Fruit):
    pass
    # Переопределение родительского метода.
    def bite(self):
        if self.has_peel:
            print('Нужно сначала очистить')
            return 
        # super -- вызовет метод у родителя.
        return super().bite()


orange = Citrus(name='orange', color='green')

# dir -- все методы и атрибуты класса.
# print(dir(orange))

# print(orange.has_peel)
# orange.clean_out()

# orange.bite()

# print(orange.has_peel)
# print(orange.value)

# orange.bite()
# print(orange.value)
# print(orange.has_peel)

# orange.bite()
# print(orange.value)
# print(orange.bite)





















# Магические методы. (Специальные методы)
# Методы класса, которы начинаются с __ и заканчиваются __
# и предоставляют возможость для взаимодействия с классом.


class Thing:
    def __init__(self, name):
        self.name = name


    def __str__(self):
        return self.name


    def __add__(self, other):
        return Thing(self.name + '-' +  other.name)


pen = Thing('pen')
string = str(pen)
# print(pen)
# print(pen)
apple = Thing('apple')

# print(apple + pen)
#
#
#
#
#
#
#
# #
# apple_pen = apple + pen
# pen_pineapple = pen + Thing('pineapple')
# #
# print(pen_pineapple + apple_pen)









# Статические методы и методы класса

class DemonstrationClass:
    name = 'Name: '

    def __init__(self, name):
        self.name += name

    # Статический метод ничего не знает о классе и о объекте, к которому он принадлежит.
    @staticmethod
    def this_is_static():
        print('Static method')

    # Метод класса знает все про класс и ничего не знает об объекте.
    @classmethod
    def this_is_class(cls):
        print(cls.name)

    def this_is_instance(self):
        print(self.name)

# DemonstrationClass.this_is_static()
# DemonstrationClass('123').this_is_static()

# DemonstrationClass.this_is_class()
d = DemonstrationClass('Test name')
print(d.name)
DemonstrationClass.name = 'Second Name:'
d2 = DemonstrationClass('Test2')
print(d2.name)


# d.this_is_class()
