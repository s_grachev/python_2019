# Exceptions -- исключения.
# Можно понимать как ошибки, возникающие во время работы кода.
# Ошибки не являются фатальными и не всегда останавливают работу программы.

# Базовый класс исключений
#


# Существует большое кол-во стандартных исключений.
# KeyError
# d = {'key': 1}
# d['key2']

# ZeroDivisionError
# 1/0

# TypeError
# print(1 + '123')




# Обработка ошибок.

x = 3
y = 3
d = {'key': 1}

# try:
#     1/x
#     f = x + y
#     d['key2']
# except ZeroDivisionError:
#     # Вызовется только в случае указанного исключения.
#     print('На 0 делаить нельзя.')
# except TypeError:
#     # Вызовется только в случае указанного исключения.
#     print('Невозможно сложить эти типы.')
# except Exception:
#     print("что-то пошло не так")
# else:
#     # Вызовется только в случае отсутвия исключения.
#     print('Ошибок не было.')
# finally:
#     # Вызовется всегда.
#     print('Все закончилось.')










def add_to(x , elements):
    for e in elements:
        try:
            x += e
        # Команда `as` записывает ошибку в переменную, с которой можно дальше работать.
        except TypeError as err:
            print('Can\'t add {}. With error: {}'.format(e, err))
            return
    print(x)


# add_to(1, [1, '23'])






# Вызов ошибок.
# raise RuntimeError('Вызванное исключение.')

# assert -  Проверка
# a = False
# assert 0




# Пишем свои исключения

# Все исключения должны быть наследованны от базового класса.
class MyError(Exception):
    pass

class ErrorConvert(MyError):
    pass

class InavalidString(MyError):
    pass

def string_to_numbers(string):
    if not string:
        raise InavalidString('Пустая или не валидная строка.')

    result = []
    for w in string:
        try:
            result.append(int(w))
        # Отлавливаем все возникшие ошибки.
        except Exception:
            raise ErrorConvert('Ошибка конвертирования на символе: {}'.format(w))
    return result

try:
    res = string_to_numbers('d34234')
    # print(res)
    pass
except MyError as error:
    print('Моя ошибка {}'.format(error))


