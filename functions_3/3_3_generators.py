l = [1, 3, 4, 5]

l_iterator = iter(l)
# print(type(l_iterator))
#
# print(next(l_iterator))
# print(next(l_iterator))
# print(next(l_iterator))



for x in l_iterator:
    # print(x)
    pass








# Можем автоматизировать?
itr = iter(range(1, 10))

def mfor(itr):
    while True:
        try:
            print(next(itr))
        except StopIteration:
            return

#mfor(itr)






# Генераторы

import random


def randoms(count):
    for x in range(count):
        yield random.randint(0, 200)
        print('yielded')


# def func(x):
#
#     return x
#
#     x += 1
#
#     return x
#
# # for r in randoms(6):
# #     print(r)
# #     pass
#
# print(type(randoms(6)))
#
#
# f = randoms(6)
# print(next(f))
# print(next(f))








def my_range(start, end, step):
    x = start
    while x < end:
        yield x
        x += step


for x in my_range(0, 100, 15):
    print(x)
    pass














