def my_sum(value_1, value_2):
    result = value_1 + value_2
    return result


result_sum = my_sum(6, 5)
my_sum(5,6)
# 
# Out:
# 11
# 
# In:

def my_sum(value_1, value_2):
    result = value_1 + value_2
    return result


result_sum = my_sum(6, 5)


print(result_sum)
# 
# Out:
# 11
# 
# In:

def empty_func():
    five = 2 + 2
    
empty_func()
a =empty_func()
a
print(a)
# 
# Out:
# None
# 
# In:

def process():
    """
    Ничего не делает
    :return: 
    """

def process():
    pass

# Значения аргументов по умолчанию
def remove_symbols(string, unresolved_chars='1234567890'):
    new_string = ''
    for c in string:
        if c not in unresolved_chars:
            new_string += c

    return new_string


origin_string = 'Enter the Numbers: 4 8 15_16 23 42'
remove_symbols(origin_string)
# 
# Out:
# 'Enter the Numbers:   _  '
# 
# In:

remove_symbols('Enter ____ ther Numbers', '_')
# 
# Out:
# 'Enter  ther Numbers'
# 
# In:

def increment_list(my_list):
    """Инкрементирует каждое число в списке.
    """

    for n in range(len(my_list)):
        my_list[n] += 1


new_list = [1, 4, 6, 7, 7, 7, 8]
increment_list(new_list)
new_list
# 
# Out:
# [2, 5, 7, 8, 8, 8, 9]
# 
# In:

def magic_func(l=['Вова', 'Петя']):
    l.append('Игоря')
    return l
magic_func()
def magic_func(l=['Вова', 'Петя']):
    l.append('Игоря')
    return l
magic_func()
magic_func()
# 
# Out:
# ['Вова', 'Петя', 'Игоря', 'Игоря']
# 
# In:

magic_func()
# 
# Out:
# ['Вова', 'Петя', 'Игоря', 'Игоря', 'Игоря']
# 
# In:

magic_func()
# 
# Out:
# ['Вова', 'Петя', 'Игоря', 'Игоря', 'Игоря', 'Игоря']
# 
# In:

magic_func()
# 
# Out:
# ['Вова', 'Петя', 'Игоря', 'Игоря', 'Игоря', 'Игоря', 'Игоря']
# 
# In:

magic_func()
# 
# Out:
# ['Вова', 'Петя', 'Игоря', 'Игоря', 'Игоря', 'Игоря', 'Игоря', 'Игоря']
# 
# In:

magic_func(['Вова'])
# 
# Out:
# ['Вова', 'Игоря']
# 
# In:

magic_func(['Вова'])
# 
# Out:
# ['Вова', 'Игоря']
# 
# In:

magic_func(['Вова'])
# 
# Out:
# ['Вова', 'Игоря']
# 
# In:

magic_func(['Вова'])
# 
# Out:
# ['Вова', 'Игоря']
# 
# In:

x = 10
def process():
    print(x)
    
process()
# 
# Out:
# 10
# 
# In:

x = 10

def process():
    x += 1    
    print(x)
        
process()
# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
#   File "<input>", line 4, in process
# UnboundLocalError: local variable 'x' referenced before assignment
# 
# In:

x = 10

def process():
    global x
    x += 1    
    print(x)
        


x = 10

def process():
    # global x
    # x += 1
    products = ['Молоко', 'Хлеб']
    
    def inner():
        print(products)
        
       
x = 10

def process():
    # global x
    # x += 1
    products = ['Молоко', 'Хлеб']
    
    def inner():
        products.pop(0)
        print(products)
        
       
process.inner()
# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
# AttributeError: 'function' object has no attribute 'inner'
# 
# In:

process().inner()
# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
# AttributeError: 'NoneType' object has no attribute 'inner'
# 

# In:

x = 10

def process():
    # global x
    # x += 1
    products = ['Молоко', 'Хлеб']
    
    def inner():
        products.pop(0)
        print(products)

    inner()
    
process()
# 
# Out:
# ['Хлеб']
# 
# In:

x = 10

def process():
    # global x
    # x += 1
    products = 2

# !!! В интерпретаторе не надо указывать nonlocal,
# если здесь убрать nonlocal, то будет ошибка

    def inner():
        nonlocal products
        products += 1
        print(products)

    inner()
    
process()
# 
# Out:
# 3
# 
# In:

process()
# 
# Out:
# 3
# 
# In:

x = 10

def process():
    # global x
    # x += 1
    products = 2
    
    def inner():
        nonlocal products
        products += 1
        print(products)

    inner()
    
process()
# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
#   File "<input>", line 12, in process
#   File "<input>", line 9, in inner
# UnboundLocalError: local variable 'products' referenced before assignment
# 
# In:

def sum1(a,b,c):
    sum(a,b,c)
    
def sum1(a,b,c):
    print(sum(a,b,c))
        
sum1(1,2,3)
# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
#   File "<input>", line 2, in sum1
# TypeError: sum expected at most 2 arguments, got 3
# 
# In:

def sum1(a,b):
    print(sum(a,b))
        
sum1(1,2)
# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
#   File "<input>", line 2, in sum1
# TypeError: 'int' object is not iterable
# 
# In:

def sum1(a,b):
    return a + b
        
sum1(1,5)
# 
# Out:
# 6
# 
# In:

def sum1(*nums):
    result = 0
    for i in nums:
        result += i
        
                


def sum1(*nums):
    result = 0
    for i in nums:
        result += i
        
def sum1(*nums):
    result = 0
    for i in nums:
        result += i
    return result

sum1(1,3,4,8,3)
# 
# Out:
# 19
# 
# In:

def sum1(*nums):
    print(nums)
    print(type(nums))
    # result = 0
    # for i in nums:
    #     result += i
    # return result

sum1(1,5,777)
# 
# Out:
# (1, 5, 777)
# <class 'tuple'>
# 
# In:

def sum1(*nums):
    result = 0
    for i in nums:
        result += i
    return result

sum1(*[1,5,6])
# 
# Out:
# 12
# 
# In:

sum1(*[1,5,6], *[5,7,8])
# 
# Out:
# 32
# 
# In:

def sum1(*nums):
    print(nums)
    result = 0
    for i in nums:
        result += i
    return result

sum1(*[1,5,6], *[5,7,8])
# 
# Out:
# (1, 5, 6, 5, 7, 8)
# 32
# 
# In:

sum1([1,5,6], [5,7,8])
# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
#   File "<input>", line 5, in sum1
# TypeError: unsupported operand type(s) for +=: 'int' and 'list'
# ([1, 5, 6], [5, 7, 8])
# 
# In:

def print_kw(*args, **kwargs):
    print(type(kwargs), end='\n' + '='*8)
    print(kwargs)
    
print_kw(1,3,4, year=2019, price=50)
# 
# Out:
# <class 'dict'>
# ========{'year': 2019, 'price': 50}
# 
# In:

def print_kw(*args, **kwargs):
    print(type(kwargs), end='\n' + '='*8 + '\n')
    print(kwargs)
    
print_kw(1,3,4, year=2019, price=50)
# 
# Out:
# <class 'dict'>
# ========
# {'year': 2019, 'price': 50}
# 
# In:

print_kw(**{'year': 2019, 'price': 50})
# 
# Out:
# <class 'dict'>
# ========
# {'year': 2019, 'price': 50}
# 
# In:

print_kw(**{'year': 2019, 'price': 50}, **{'count': 4})
# 
# Out:
# <class 'dict'>
# ========
# {'year': 2019, 'price': 50, 'count': 4}
# 
# In:

print_kw(**{'year': 2019, 'price': 50}, count=5)
# 
# Out:
# <class 'dict'>
# ========
# {'year': 2019, 'price': 50, 'count': 5}
# 
# In:

print_kw(**{'year': 2019, 'price': 50}, count={'': 1})
# 
# Out:
# <class 'dict'>
# ========
# {'year': 2019, 'price': 50, 'count': {'': 1}}
# 
# In:

def multiply_string(string):
    return string * 3

def cut_string(string):
    return string[1:-1]

def invert_string(string):
    return string[::-1]

print(type(multiply_string))
# 
# Out:
# <class 'function'>
# 
# In:

callable(multiply_string)
# 
# Out:
# True
# 
# In:

callable(55)
# 
# Out:
# False
# 
# In:

def do_with_string(s, *args):
    print(s)
    for f in args:
        if not callable(f):
            continue
        s = f(s)
        print(s)
        
do_with_string('Сыворотка из под простокваши')
# 
# Out:
# Сыворотка из под простокваши
# 
# In:

do_with_string('Сыворотка из под простокваши', multiply_string, cut_string)
# 
# Out:
# Сыворотка из под простокваши
# Сыворотка из под простоквашиСыворотка из под простоквашиСыворотка из под простокваши
# ыворотка из под простоквашиСыворотка из под простоквашиСыворотка из под простокваш
# 
# In:

do_with_string('Сыворотка из под простокваши', *(multiply_string, cut_string))
# 
# Out:
# Сыворотка из под простокваши
# Сыворотка из под простоквашиСыворотка из под простоквашиСыворотка из под простокваши
# ыворотка из под простоквашиСыворотка из под простоквашиСыворотка из под простокваш
# 
# In:

do_with_string('Сыворотка из под простокваши', (multiply_string, cut_string))
# 
# Out:
# Сыворотка из под простокваши
# 
# In:


def do_with_string(s, *args):
    print(s)
    for f in args:
        if not callable(f):
            continue
        s = f(s)
        print(s)


def do_with_string(s, *args):
    print(s)
    print('args', args)
    for f in args:
        if not callable(f):
            continue
        s = f(s)
        print(s)

do_with_string('Сыворотка из под простокваши', (multiply_string, cut_string))
# 
# Out:
# Сыворотка из под простокваши
# args ((<function multiply_string at 0x10cca6d40>, <function cut_string at 0x10cca6b90>),)
# 
# In:

def run_with_information(fn, *args, **kwargs):
    print('Начало выполнения')
    print('Неименованные аргументы: {}'.format(args))
    print('Именованные аргументы: {}'.format(kwargs))
    result = fn(*args, **kwargs)
    print('Окончание выполнения')
    print('Результат: {}'.format(result))
    return result

def sum_elemenst(n):
    return sum(range(n + 1))

run_with_information(sum_elemenst)
# 
# Out:
# Начало выполнения
# Неименованные аргументы: ()
# Именованные аргументы: {}
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
#   File "<input>", line 5, in run_with_information
# TypeError: sum_elemenst() missing 1 required positional argument: 'n'
# 
# In:

run_with_information(sum_elemenst, 1)
# 
# Out:
# Начало выполнения
# Неименованные аргументы: (1,)
# Именованные аргументы: {}
# Окончание выполнения
# Результат: 1
# 1
# 
# In:

sum_elements = run_with_information(sum_elemenst, 4)
# 
# Out:
# Начало выполнения
# Неименованные аргументы: (4,)
# Именованные аргументы: {}
# Окончание выполнения
# Результат: 10
# 
# In:

sum_elements
# 
# Out:
# 10
# 
# In:

def print_information_decorator(fn):
    def wrapper(*args, **kwargs):
        print('Начало выполнения')
        print('Не именнованые аргументы: {}'.format(args))
        print('Именнованные аргументы: {}'.format(kwargs))
        result = fn(*args, **kwargs)
        print('Окончание выполнения')
        print('Результат: {}'.format(result))
        return result

    return wrapper

print_information_decorator(sum_elemenst)
# 
# Out:
# <function print_information_decorator.<locals>.wrapper at 0x10cca67a0>
# 
# In:

sum_elemenst = print_information_decorator(sum_elemenst)
sum_elemenst
# 
# Out:
# <function print_information_decorator.<locals>.wrapper at 0x10cc764d0>
# 
# In:

sum_elemenst(5)
# 
# Out:
# Начало выполнения
# Не именнованые аргументы: (5,)
# Именнованные аргументы: {}
# Окончание выполнения
# Результат: 15
# 15
# 
# In:

def print_information(fn):
    def wrapper(*args, **kwargs):
        print('Начало выполнения')
        print('Не именнованые аргументы: {}'.format(args))
        print('Именнованные аргументы: {}'.format(kwargs))
        result = fn(*args, **kwargs)
        print('Окончание выполнения')
        print('Результат: {}'.format(result))
        return result

    return wrapper


@print_information
def sum_elemenst(n):
    return sum(range(n + 1))

sum_elemenst(5)
# 
# Out:
# Начало выполнения
# Не именнованые аргументы: (5,)
# Именнованные аргументы: {}
# Окончание выполнения
# Результат: 15
# 15
# 
# In:

# Декоратор с аргуметами
def if_more(num):
    def decorator(fn):
        def wrapper(x):
            if x < num:
                print('{} меньше {}'.format(x, num))
                return
            else:
                return fn(x)

        return wrapper
    return decorator

@if_more(10)
def sum_elemenst(n):
    return sum(range(n + 1))

sum_elemenst(15)
# 
# Out:
# 120
# 
# In:

sum_elemenst(1)
# 
# Out:
# 1 меньше 10
# 
# In:


@print_information
@if_more(12)
def sum_elemenst(n):
    return sum(range(n + 1))

@if_more(34)
def minus_elem():
    pass

sum_elemenst(15)
# 
# Out:
# Начало выполнения
# Не именнованые аргументы: (15,)
# Именнованные аргументы: {}
# Окончание выполнения
# Результат: 120
# 120
# 
# In:

sum_elemenst(10)
# 
# Out:
# Начало выполнения
# Не именнованые аргументы: (10,)
# Именнованные аргументы: {}
# 10 меньше 12
# Окончание выполнения
# Результат: None
# 
# In:

# Декортаторы так же работают с классами
def yeah_new_object(cls):
    def wrapper(*args, **kwargs):
        print('YEAH!! THIS IS NEW OBJECT!')
        return cls(*args, **kwargs)
    return wrapper



@yeah_new_object
class MyClass:
    pass


c = MyClass()
# 
# Out:
# YEAH!! THIS IS NEW OBJECT!
# 
# In:

def class_decorator(cls):
    def wrapper(*args, **kwargs):
        print('YEAH!! THIS IS NEW OBJECT!')
        new_cls = cls(*args, **kwargs)

        import datetime

        new_cls.create_date = datetime.datetime.now()
        return new_cls

    return wrapper


@class_decorator
class MyClass:
    pass

c = MyClass()
# 
# Out:
# YEAH!! THIS IS NEW OBJECT!
# 
# In:

c.create_date
# 
# Out:
# datetime.datetime(2019, 7, 17, 19, 23, 46, 59326)
# 
# In:

c.create_date.isoformat()
# 
# Out:
# '2019-07-17T19:23:46.059326'
# 
# In:

l = [1, 3, 4, 5]

l_iterator = iter(l)
l_iterator
# 
# Out:
# <list_iterator object at 0x10cc8c990>
# 
# In:

type(l_iterator)
# 
# Out:
# <class 'list_iterator'>
# 
# In:

next(l_iterator)
# 
# Out:
# 1
# 
# In:

next(l_iterator)
# 
# Out:
# 3
# 
# In:

next(l_iterator)
# 
# Out:
# 4
# 
# In:

l_iterator.__next__()
# 
# Out:
# 5
# 
# In:

next(l_iterator)


for x in l_iterator:
    pass   

# 
# Out:
# Traceback (most recent call last):
#   File "<input>", line 1, in <module>
# StopIteration
# 
# In:

l_iterator = iter([1,2,3,3,5,3,1])

for x in l_iterator:
    print(x)   

# 
# Out:
# 1
# 2
# 3
# 3
# 5
# 3
# 1
# 
# In:

import random


def randoms(count):
    for x in range(count):
        yield random.randint(0, 200)
        
def randoms(count):
    for x in range(count):
        yield random.randint(0, 200)
        print('yielded')
        
randoms(3)
# 
# Out:
# <generator object randoms at 0x10cc61dd0>
# 
# In:

for n in randoms(3):
    print(n)
    
# 
# Out:
# 179
# yielded
# 181
# yielded
# 89
# yielded
# 
# In:

def my_range(start, end, step):
    x = start
    while x < end:
        yield x
        x += step
        
for x in my_range(0, 100, 15):
    print(x)
    
# 
# Out:
# 0
# 15
# 30
# 45
# 60
# 75
# 90
# 
# In:

func = lambda num1, num2: num1 == num2
func
# 
# Out:
# <function <lambda> at 0x10cc73950>
# 
# In:

type(func)
# 
# Out:
# <class 'function'>
# 
# In:

func(12, 40)
# 
# Out:
# False
# 
# In:

def f(x, y):
    return x + y


d = {
    'plus': f,
    'minus': lambda x, y: x - y,
    'mult': lambda x, y: x * y,
}
print(d)
print(d['plus'](4, 5))
print(d['minus'](4, 5))
print(d['mult'](4, 5))
print(d)

# 
# Out:
# {'plus': <function f at 0x10cc73e60>, 'minus': <function <lambda> at 0x10cc739e0>, 'mult': <function <lambda> at 0x10cc738c0>}
# 9
# -1
# 20
# {'plus': <function f at 0x10cc73e60>, 'minus': <function <lambda> at 0x10cc739e0>, 'mult': <function <lambda> at 0x10cc738c0>}
# 
# In:

d
# 
# Out:
# {'plus': <function f at 0x10cc73e60>, 'minus': <function <lambda> at 0x10cc739e0>, 'mult': <function <lambda> at 0x10cc738c0>}
# 
# In:

from pprint import pprint
pprint(d)
# 
# Out:
# {'minus': <function <lambda> at 0x10cc739e0>,
#  'mult': <function <lambda> at 0x10cc738c0>,
#  'plus': <function f at 0x10cc73e60>}
# 
# In:

d['minus']
# 
# Out:
# <function <lambda> at 0x10cc739e0>
# 
# In:

d['minus'](30, 10)
# 
# Out:
# 20
# 
# In:

d['mult'](4, 5)
# 
# Out:
# 20
# 
# In:

func  = lambda arg1, arg2: arg1 == arg2
func('4', 122)
# 
# Out:
# False
# 
# In:

func(4, 4.0)
# 
# Out:
# True
