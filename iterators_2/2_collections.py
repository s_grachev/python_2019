# Списки!

l = [1, 3, 4, 5, 56]
# print(type(l))














# Срезы
l = [1, 3, 4, 5, 6, 7, 8]
# print(l[0])
# print(l[::-1])













# Добавляем элементы
ls = ['F', 's', 3,3, 4.0]
ls.append('Added')
# print(ls)

ls.extend(['E', 'D', 3])

ls2 = ls[:]
# print(ls2)
ls2.append("added2")
# print(ls)







new_ls = ls + ['B', 'C']
# print(ls)
# print(new_ls)












# Определяем длину списка
# print(len(ls))














# Количество элементов в списке
l = [True, False, True, False, False]
# print(l.count(True))












# Проверка элемента в списке
l = ['This', 'is', 'Sparta']
# print('Leonid' in l)
# print('Sparta' in l)

# print(l.index('is'))














# Удаление элементов из списка.
l = ['one', 'two', 'three', 'four', 'five']
# print(l)
# print(l[2])
# del l[2:]

# print(l)
# print(l)
# print(l.pop(2))
# print(l)






l = ['one', 'two', 'three', 'four', 'five', 'one']

l.remove('one')
# print(l)

# Сортировка
so = sorted([3, 45, 1.0,  True, 56, 3, 1])
# print(so)













# Список как булевое значение

# print(bool([]))
# print(bool([1, 2]))









# генерация списков
new_list2 = [x for x in range(4)]


# print(range(20))
# print(new_list)

l3 = [1,2,4,4,1,1,1,1,123,423]
# Генерация списков с условиями
new_list = [x for x in new_list2 if not x%2]

# print([x for x in l3 if x!=1])















# Кортежи
t = (1, 4, 45, 'qwe', 'asd')
# print(type(t))
#
# print(t[1: -2])
#
# print(t + (34,))
#
# print('qwe' in t)




# Конвертации
t_to_l = list((12, 323, 434))
l_to_t = tuple([12, 323, 434])
# print(t_to_l)
# print(type(t_to_l))
# print(l_to_t)
# print(type(l_to_t))








# Множественное присваивание
v = (1, 2, 3)
x, s, _ = v
# print(x)
# print(s)
# print(l)
# print(_)

d = 1
a = 2
d, a = a, d
# print(d)
# print(a)













# a, s, d = 3, 4, 5
# print(a)
# print(s)
# print(d)
d = 4,
# print(d)

















# Сеты

s = {1, 2, 3, '12', '4'}
# print(type(s))

s = set()
s = {}


sе = {2, 3, 1}
# print(sе)

# Срезы не будут работать
# print(s[1:3])

# Длина
se = {1, 2, 3, 1}
# print(len(sе))


# Добавление элементов
se = {1, 2, 3, 1}
se.add(2)
se.add(5)
se.add(4)
# print(se)











se.update({2, 4, 10, 11})
# print(se)



















se = {1, 2, 3, 1}


# Удаление элементов
# se.remove(4)
se.discard(10)
# print(se)







# Конвертация
l = [1, 2, 'asd', 'asd', 1, 'asd']
# print(set(l))


# Операциии со множеством

s_1 = {'a', 'b', 'c', 'd'}
s_2 = {'a', 'e', 'c', 'g', 'l'}

# Объединение
# print(s_1.union(s_2))

# Общие элементы
# print(s_1.intersection(s_2))

# Различия
# print(s_1.difference(s_2))
# print(s_2.difference(s_1))
# print(s_1.symmetric_difference(s_2))
