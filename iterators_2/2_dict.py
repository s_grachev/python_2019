# Словари

d = {
    'key1': 'value1',
    2: 'Value2',
    'key3': [1, 2, 3]
}
# d = {1: 2}
# print(type(d))

d = dict(key1='value1', key2='value2')

# print(d)
# получение значений по ключу

# print(d['key1'])










man = {'Имя': 'Вася', 'Фамилия': 'Сидоров'}
man['Возраст'] = 3
# print(man)

man['Возраст'] = 4
# print(man)



# Словарь может хранить разные значения
c = ['yellow', 'blue', 'green']
d = {
    'colors': c,
    'numbers': [1, 4, 5],
    'user': {'Имя': 'Вася', 'Фамилия': 'Сидоров'}
}
# print(d)
# print(d['user']['Имя'])


del d['colors']
# print(d)
# del d['colors'][2]
# print(d)
# d.pop('user')
# print(d)






# Длина словаря
l = len(d)
# print(l)









# Получение ключей словаря

d = {
    'colors': ['yellow', 'blue', 'green'],
    'numbers': [1, 4, 5],
    'user': {'Имя': 'Вася', 'Фамилия': 'Сидоров'}
}

import pprint
# print(d.keys())
# print(d.values())
# pprint.pprint(d)



# Проверка ключей в словаре

result = 'colors' in d
result2 = 'key1' not in d
# print(result)
# print(result2)



# Изменяемый тип данных.

d = {'key1': 'Hello'}
a = d.copy()
d['123'] = 1
# print(a)

a['key2'] = 'world'
# print(d)












d = {'key1': 'Hello'}
a = d.copy()
# print(a)
a['key2'] = 'world'
# print(d)
# print(a)



# Безопасный способ получить элемент
d = {
    'city': 'Moscow',
    'country': 'Russia'
}
val = d.get('city', 'Ulsk')
# print(val)












# Генерация словарей

new_d = {x:'x**2' for x in range(12)}
# pprint.pprint(new_d)



















# Словарь в булевом виде

# print(bool({}))
# print(bool({1: 3}))


























# s = set()
# d = {}

# s1 = {1, 2,}
# d = {1: 2}

