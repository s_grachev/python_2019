# Циклы

for n in range(5, 15, 2):
    # print(n)
    pass


# Цикл может итерироваться по любой коллекции
l = ['guido', 'van', 'rossum']
for w in l:
    # print(w)
    pass


#
d = {1: 3, '2': 'sd'}
#
for k, v in d.items():
    # print(k)
    # print(v)
    # print()
    pass


# Можно создавать вложенные циклы
for g in l:
    for w in '12345679':
        # print(g * int(w))
        pass


matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
for x in matrix:
    for el in x:
        # print(el)
        pass


# цикл while
i = 20
while i > 10:
    i -= 1
    # print(i)

# print('Result = {}'.format(i))

# while True:
    # print(1)
    # pass


# Команды break и continue

# Прерывает выполнение цикла
for x in range(15):
    if x == 5:
        break
    # print(x)

# Переходит к след элементу
for x in 'Hello world':
    if x in ('e', 'o'):
        continue
        pass
    # print(x)


#  else выполняется если функция завершилась без прерывания
names = ['Андрей', 'Иван', 'Марина', ' Кирилл']
for name in names:
    # print('Name: {}'.format(name))
    if name == 'Иван':
        # print('Иван найден')
        break
else:
    # print('Мы не нашли Иван')
    pass


#  Итерируемся по словарю
bigest_city = {
    'Шанхай': 24256800,
    'Карачи': 23500000,
    'Пекин': 21516000,
    'Дели': 16349831,
    'Лагос': 16060303,
}

for x in bigest_city:
    # print(bigest_city[x])
    pass


l = ['a', 'b', 'c', 'd', 'e']
for i, x in enumerate(l):
    print(i)
    print(x)
    print()
    pass
