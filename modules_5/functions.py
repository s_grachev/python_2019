from typing import List, Dict


def fib(n: int):
    a, b = 0, 1
    while b < n:
        print(b)
        a, b = b, a + b
    print()


def get_string_array() -> List[str]:
    return ['te', 'ew']


def multiply_string(string: str) -> str:
    return string * 3


def cut_string(string: str) -> str:
    return string[1:-1]


def invert_string(string: str) -> str:
    return string[::-1]


print('My name is {}'.format(__name__))
