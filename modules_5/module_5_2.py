# Импорты своих модулей
# from modules_5 import functions
import functions
# print(dir(functions))

# Важно помнить! Импортируемый файл всегда исполняется при импорте.
from modules_5.functions import fib

# print(fib(3))


# Пути для импорта.
import sys
import pprint

pprint.pprint(sys.path)