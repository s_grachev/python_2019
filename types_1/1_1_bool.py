# # Неизменяемые
# float
# int
# str
# tuple
# bool
#
# # Изменяемые
# list
# dict
# set
















first = True
second_variable = False

# C помощью команды type можно получить тип любой переменной.
typer = type(second_variable)
# print(typer)


# В Python булевые значения могут обрабатываться как числа.
int_true = int(True)
int_false = int(False)

# print(int_true)
# print(int_false)

# Поэтому с ними можно выполнять математические операции.
result_sum = True + False
result_sum_true = True + True
result_div = True - True

# print(result_sum)
# print(result_sum_true)
# print(result_div)


# Можно проверить тип любой переменной. Это работат с любыми типами.
first = True
is_bool = isinstance(first, str)
# print(is_bool)


# Можно вывести сразу какое-либо значение, без присваевания.
# print(isinstance(first, str))

# Булевые операторы
# print(True or False)
# print(False or False)
# print(True and False)
# print(not False)
# print(not True or False)


# None - отсутвие какого-то значения.
none_value = None
# print(type(none_value))
