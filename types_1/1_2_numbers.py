# Объявляем переменную int
five = 5
# print(type(five))


# Объявляем переменную float
six_point_two = 5.2
# print(type(six_point_two))


# Комплексное число
complex_num = 1 + 3j
# print(type(complex_num))
# print(complex_num)


# Выводим реальную и мнимую часть числа.
# print(complex_num.real)
# print(complex_num.imag)


# Конвертация
# print(float(8))
# print(int(-6.934))
# print(float("32.45"))
# print(bool(0))
# print(bool(1))
# print(bool(None))


# В типе float числа после точки точны до 15 символа
# print(-1.12345678901234561890)
# Decimal
# CPython

# Операции с числами
# print(3 + 5)
# print(5 - 3)
# print(5.0 * 3)

x = -3
# print(-x)

# Деление с точкой
result_point = 14 / 2

# print(result_point)
# print(int(result_point))
# print(type(result_point))

# Деление без точки c округлением.
result_wpoint = 13 // 2

# print(result_wpoint)
# print(type(result_wpoint))


# Деление не всегда может быть без точки.
result_point = 13.0 // 2

# print(result_point)
# print(type(result_point))


# Остаток от деления
# print(14 % 4)

# Возведение в степень
print(4 ** 3)

