# Все строки юникодовые
single = 'single qoutes text'
double = "double qoutes text"
# print(single)

triple_single = '''triple single quotes text'''
triple_double = """triple
 single quotes textend"""


# print(double)
# print(triple_double)
# print()
# print(triple_single)
# print(triple_double)
#


# Печатное представление строки (сырая строка)
# print(r'first row \n second row')
# import this


# Контетенация
cont_string = "First part" "Second part"
# print(cont_string)

# Операции со строками
# print("First part" + "Second part")
# print("10" + "5")
# print("First part" - "Second part")
# print("First" * 4)

l = len("Строка")
# print(l)

# Поиск по строке
s = "  Hello world!!!!!!!"
# print(s.rfind("o"))
# print("H" in s)


# Проверки
# print(s.startswith('1'))
# print(s.endswith('!'))
# print("f".islower())
# print("f".isupper())
# print("4.8".isdigit())

# Убираем пробел в начале строки
# print(s)
# print(s.strip())
# print(s.strip('!'))
# print(s.strip('!').strip())
# print(s)

# Заменяем символы. Важно получаем новую строку.
s = "  Hello world?!"
# print(s)
# print(s.replace("  ", ""))
# print(s)

# s = s.replace('l', '1')
# print(s)

# нижний ВЕРХНИЙ регистр
# print(s.lower())
# print(s.upper())

# Преобразование строки в список
s_point = 'a.b.a.c.1231231'
# print(s_point)
# print(s_point.split('.'))


name = "sdfsdf"
# Форматирование строк
format_string = 'Привет, %s. Сегодня %d.02.2018' % (name, 5)
# print(format_string)

format_string_2 = 'Привет, {}. Сегодня {}.02.2018'.format('Михаил', 20)
# print(format_string_2)

format_string_3 = 'Привет, {name}. Сегодня {day}.02.2018'.format(day=22, name='Петр')
# print(format_string_3)
# day = 33
# name = "A"
# format_string_4 = f'Привет, {name}. Сегодня {day}.02.2018'
# print(format_string_4)

# Срезы
s = "Don't panic!"
# print(s[0])
# print(s[2:5])
# print(s[:])
# print(s[-2])
print(s[1:5:-1])



# конвернтация строк в булевые значения

# print(bool(''))
# print(bool(None))
# print(bool('3123'))
